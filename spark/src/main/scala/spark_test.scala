import org.apache.spark.sql.{DataFrame, SparkSession}
import org.apache.spark.sql.types._
import org.apache.spark.sql.functions._
import scala.reflect.ClassTag


case class Gener(id:Integer,
                 g2:Option[Double],
                 g3:Option[Double],
                 g4:Option[Double])



object spark_test extends App{
  def cast[T: ClassTag](o: Any): Option[T] = o match {
    case v: T => Some(v)
    case _ => None
  }

  val spark = SparkSession.builder.appName("Simple Application").getOrCreate()

  val sc = spark.sparkContext
  val path="/home/vlad/Doc/contest/mlbotcamp6/dataset/bs_avg_kpi.csv"

  val chnn_schema = StructType(
    List(
      StructField("T_DATE",StringType , false),
      StructField("CELL_LAC_ID",IntegerType , false),
      StructField("CELL_AVAILABILITY_2G",DoubleType , true),
      StructField("CELL_AVAILABILITY_3G",DoubleType , true),
      StructField("CELL_AVAILABILITY_4G",DoubleType , true),
      StructField("CSSR_2G",DoubleType , true),
      StructField("CSSR_3G",DoubleType , true),
      StructField("ERAB_PS_BLOCKING_RATE_LTE",DoubleType , true),
      StructField("ERAB_PS_BLOCKING_RATE_PLMN_LTE",DoubleType , true),
      StructField("ERAB_PS_DROP_RATE_LTE",DoubleType , true),
      StructField("HSPDSCH_CODE_UTIL_3G",DoubleType , true),
      StructField("NODEB_CNBAP_LOAD_HARDWARE",DoubleType , true),
      StructField("PART_CQI_QPSK_LTE",DoubleType , true),
      StructField("PART_MCS_QPSK_LTE",DoubleType , true),
      StructField("PROC_LOAD_3G",DoubleType , true),
      StructField("PSSR_2G",DoubleType , true),
      StructField("PSSR_3G",DoubleType , true),
      StructField("PSSR_LTE",DoubleType , true),
      StructField("RAB_CS_BLOCKING_RATE_3G",DoubleType , true),
      StructField("RAB_CS_DROP_RATE_3G",DoubleType , true),
      StructField("RAB_PS_BLOCKING_RATE_3G",DoubleType , true),
      StructField("RAB_PS_DROP_RATE_3G",DoubleType , true),
      StructField("RBU_AVAIL_DL",DoubleType , true),
      StructField("RBU_AVAIL_DL_LTE",DoubleType , true),
      StructField("RBU_AVAIL_UL",DoubleType , true),
      StructField("RBU_OTHER_DL",DoubleType , true),
      StructField("RBU_OTHER_UL",DoubleType , true),
      StructField("RBU_OWN_DL",DoubleType , true),
      StructField("RBU_OWN_UL",DoubleType , true),
      StructField("RRC_BLOCKING_RATE_3G",DoubleType , true),
      StructField("RRC_BLOCKING_RATE_LTE",DoubleType , true),
      StructField("RTWP_3G",DoubleType , true),
      StructField("SHO_FACTOR",DoubleType , true),
      StructField("TBF_DROP_RATE_2G",DoubleType , true),
      StructField("TCH_DROP_RATE_2G",DoubleType , true),
      StructField("UTIL_BRD_CPU_3G",DoubleType , true),
      StructField("UTIL_CE_DL_3G",DoubleType , true),
      StructField("UTIL_CE_HW_DL_3G",DoubleType , true),
      StructField("UTIL_CE_UL_3G",DoubleType , true),
      StructField("UTIL_SUBUNITS_3G",DoubleType , true),
      StructField("UL_VOLUME_LTE",DoubleType , true),
      StructField("DL_VOLUME_LTE",DoubleType , true),
      StructField("TOTAL_DL_VOLUME_3G",DoubleType , true),
      StructField("TOTAL_UL_VOLUME_3G",DoubleType , true)
    ))

  val df = spark.read
    .format("csv")
    .option("header", "true") //reading the headers
    .schema(chnn_schema)
    .option("delimiter", ";")
    .load(path)

  
  //Добавим месяц
  val named_column = Array("day","month")
  val dfColumnNames = df.columns
  val df_dm = df.withColumn("temp", split(col("T_DATE"), "\\.")).select(
    dfColumnNames.map(c => col(c))++(0 until 2).map(i => col("temp").getItem(i)
  .as(named_column(i))): _*)
  
//
//  val r = df_dm.filter(df_dm.col("month")===5).select("CELL_LAC_ID","CELL_AVAILABILITY_2G",
//          "CELL_AVAILABILITY_3G",
//          "CELL_AVAILABILITY_4G")

  val r = df_dm.select("CELL_LAC_ID","CELL_AVAILABILITY_2G",
    "CELL_AVAILABILITY_3G",
    "CELL_AVAILABILITY_4G")

  val g_type = r.rdd.map(x => {
    val t = x.toSeq
    Gener(
      t(0).asInstanceOf[Integer],
      cast[Double](t(1)),
      cast[Double](t(2)),
      cast[Double](t(3))
    )
  })
    .map {
      case Gener(id, _, _, Some(_)) => (id, 4)
      case Gener(id, _, Some(_), _) => (id, 3)
      case Gener(id, Some(_), _, _) => (id, 2)
      case Gener(id, _, _, _) => (id, 0)
    }
    .map(x=>x._1.toString+","+x._2.toString)
    .collect

  import java.io._
  val file = new File("type.csv")
  val bw = new BufferedWriter(new FileWriter(file))

  g_type.foreach(line => bw.write(s"$line\n"))
  bw.close()

  sc.stop()
  spark.stop()
}
